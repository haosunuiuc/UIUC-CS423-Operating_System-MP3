#define LINUX

#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <linux/mm.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

#include "mp3_given.h"

#define DEBUG                     1
#define DIRECTORY                 "mp3"
#define FILENAME                  "status"
#define REGISTER                  'R'
#define UNREGISTER                'U'
#define NPAGES                    128
#define SAMPLEINTERVAL            50
#define PROFILER_BUFFER_LENGTH    (NPAGES * PAGE_SIZE / sizeof(unsigned long))
#define CHAR_DEVICE_NAME          "mp3_char_device"

struct mp3_task_struct{
    struct task_struct *linux_task;
    struct list_head list;
    unsigned int pid;

};

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_entry;
static struct list_head mp3_task_struct_list_head;
static struct workqueue_struct *mp3_work_queue;
static struct delayed_work *mp3_work;
static unsigned long *mp3_profiler_buffer;
static int mp3_profiler_buffer_ptr = 0;
static dev_t mp3_dev_t; //used to hold major and minor number of char device
static struct cdev mp3_cdev;
static unsigned long accumulative_minor_fault_count = 0;
static unsigned long accumulative_major_fault_count = 0;
static unsigned long accumulative_cpu_time = 0;

DEFINE_MUTEX(list_mutex);

static void mp3_work_function(struct work_struct *work){
    printk("mp3_work_function called\n");
    unsigned long minor_fault = 0, major_fault = 0, user_time = 0, sys_time = 0;
    unsigned long total_minor_fault = 0, total_major_fault = 0, total_cpu_time = 0;
    struct mp3_task_struct *tmp;

    mutex_lock_interruptible(&list_mutex);
    list_for_each_entry(tmp, &mp3_task_struct_list_head, list){
        if(get_cpu_use(tmp->pid, &minor_fault, &major_fault, &user_time, &sys_time) == -1){
            continue;
        }
        total_minor_fault += minor_fault;
        total_major_fault += major_fault;
        total_cpu_time += user_time + sys_time;
    }
    mutex_unlock(&list_mutex);

    /* update accumulative values */
    accumulative_minor_fault_count += total_minor_fault;
    accumulative_major_fault_count += total_major_fault;
    accumulative_cpu_time += total_cpu_time;

    mp3_profiler_buffer[mp3_profiler_buffer_ptr] = jiffies;
    printk("jiffies %lu\n", mp3_profiler_buffer[mp3_profiler_buffer_ptr]);
    mp3_profiler_buffer_ptr++;
    mp3_profiler_buffer[mp3_profiler_buffer_ptr] = accumulative_minor_fault_count;
    printk("total minor fault %lu\n", mp3_profiler_buffer[mp3_profiler_buffer_ptr]);
    mp3_profiler_buffer_ptr++;
    mp3_profiler_buffer[mp3_profiler_buffer_ptr] = accumulative_major_fault_count;
    printk("total major fault %lu\n", mp3_profiler_buffer[mp3_profiler_buffer_ptr]);
    mp3_profiler_buffer_ptr++;
    mp3_profiler_buffer[mp3_profiler_buffer_ptr] = jiffies_to_msecs(cputime_to_jiffies(accumulative_cpu_time));
    printk("total cpu time %lu\n", mp3_profiler_buffer[mp3_profiler_buffer_ptr]);
    mp3_profiler_buffer_ptr++;

    if(mp3_profiler_buffer_ptr >= PROFILER_BUFFER_LENGTH){
        mp3_profiler_buffer_ptr = 0;
    }

    queue_delayed_work(mp3_work_queue, mp3_work, msecs_to_jiffies(SAMPLEINTERVAL));

    printk("mp3_work_function ended\n");

}

void mp3_register_process(char *buf){
    printk(KERN_INFO "********************   mp3 module register process   ********************\n");

    /* Allocate a tmp mp3_task_struct for the new process */
    struct mp3_task_struct *tmp;
    tmp = (struct mp3_task_struct*) kmalloc(sizeof(struct mp3_task_struct), GFP_KERNEL);

    /* Save process pid info */
    sscanf(buf, "%u", &tmp->pid);

    /* Find linux PCB*/
    tmp->linux_task = find_task_by_pid(tmp->pid);

    /**/
    if(list_empty(&mp3_task_struct_list_head)){
        printk("mp3_task_struct_list is empty\n");
        queue_delayed_work(mp3_work_queue, mp3_work, msecs_to_jiffies(SAMPLEINTERVAL));
    }
    
    /* Add task to task list */
    mutex_lock_interruptible(&list_mutex);
    list_add(&(tmp->list), &(mp3_task_struct_list_head));
    mutex_unlock(&list_mutex);

    struct mp3_task_struct *cursor;
    list_for_each_entry(cursor, &mp3_task_struct_list_head, list){
        printk("process %u is in the list\n", cursor->pid);
    }

}

void mp3_unregister_process(char *buf){
    printk(KERN_INFO "********************   mp3 module unregister process   ********************\n");

    /*Save the pid of the process which is unregistering*/
    unsigned int pid;
    sscanf(buf, "%u", &pid);
    /*Find the process in the list*/
    struct mp3_task_struct *tmp, *cursor;
    mutex_lock_interruptible(&list_mutex);
    list_for_each_entry(cursor, &mp3_task_struct_list_head, list){
        if (cursor->pid == pid) {
            tmp = cursor;
        }
    }
    list_del(&tmp->list);
    mutex_unlock(&list_mutex);
    // free tmp
    kfree(tmp);

    if(list_empty(&mp3_task_struct_list_head)){
        printk("list is empty\n");
        cancel_delayed_work(mp3_work);
        flush_workqueue(mp3_work_queue);
    }

}

static ssize_t mp3_read(struct file *file, char __user *buffer, size_t count, loff_t *data){
    printk(KERN_ALERT "mp3_read called\n");

    static int finish = 0;
    static ssize_t notRead;

    /* Check the indicator */
    if(finish){
        finish = 0;
        return 0;
    }

    int copied = 0;
    char *buf;
    buf = (char *) kmalloc(count, GFP_KERNEL);
    struct mp3_task_struct *tmp;

    /* Enter critical section */
    mutex_lock_interruptible(&list_mutex);
    list_for_each_entry(tmp, &mp3_task_struct_list_head, list){
        copied += sprintf(buf + copied, "pid:%u\n", tmp->pid);
    }
    mutex_unlock(&list_mutex);
    buf[copied] = '\0';

    /* Perform copying to user space */
    if((notRead = copy_to_user(buffer, buf, copied)) != 0){
        printk("read error\n");
        return -EFAULT;
    }

    /* Set indicator on success */
    if (notRead == 0) {
        finish = 1;
    }
    kfree(buf);
    return copied;
}

static ssize_t mp3_write(struct file *file, const char __user *buffer, size_t count, loff_t *data){
    printk(KERN_ALERT "mp3_write called\n");

    /* Get the message from user space */
    char *buf;
    buf = (char *) kmalloc(count + 1, GFP_KERNEL);
    copy_from_user(buf, buffer, count);
    buf[count] = '\0';

    switch(buf[0]){
        case REGISTER:
            mp3_register_process(buf + 2);
            break;
        case UNREGISTER:
            mp3_unregister_process(buf + 2);
            break;
        default:
            printk(KERN_INFO "error in switch case\n");
    }

    kfree(buf);
    return count;
}

static int mp3_char_device_open(struct inode *_inode, struct file *_file){
    printk("char device file has been opened\n");
    return 0;

}

static int mp3_char_device_release (struct inode *_inode, struct file *_file){
    printk("char device file has been released\n");
    return 0;

}

/*
 * This method will be called when an user-space process calls system call
 * mmap(). This method will build a new page table for the calling process.
 * Page by page, each page of the profiler buffer area allocated by vmalloc
 * will be first mapped to a physical page frame number, using vmalloc_to_-
 * pfn. Then this page(physical page) will be inserted into the page table-
 * of the vma structure of the process.
 * Return 0 on success. Return a negative number(-EIO) if fail.
 */
static int mp3_char_device_mmap(struct file *fp, struct vm_area_struct *vma){
    unsigned long unmapped_buffer_length = vma->vm_end - vma->vm_start;
    //struct page *page;
    unsigned long pfn;
    char *tmp_profiler_buffer_ptr = (char *) mp3_profiler_buffer;
    unsigned long vma_insert_index = vma->vm_start;
    int ret;

    if(unmapped_buffer_length > NPAGES * PAGE_SIZE){
        printk("vma length is bigger than profiler buffer size\n");
        return -EIO;
    }

    /*
    while(unmapped_buffer_length > 0){
        page = vmalloc_to_page(tmp_profiler_buffer_ptr);
        if((ret = vm_insert_page(vma, vma_insert_index, page)) < 0){
            printk("Isert page into vma failed\n");
            return -EIO;
        }
        vma_insert_index += PAGE_SIZE;
        tmp_profiler_buffer_ptr += PAGE_SIZE;
        unmapped_buffer_length -= PAGE_SIZE;
    }
    */

    while(unmapped_buffer_length > 0){
        pfn = vmalloc_to_pfn(tmp_profiler_buffer_ptr);
        if((ret = remap_pfn_range(vma, vma_insert_index, pfn, PAGE_SIZE, vma->vm_page_prot)) < 0){
            printk("Insert page into vma failed\n");
            return -EIO;
        }
        vma_insert_index += PAGE_SIZE;
        tmp_profiler_buffer_ptr += PAGE_SIZE;
        unmapped_buffer_length -= PAGE_SIZE;

    }
    return 0;

}

static const struct file_operations mp3_fileoperation = {
        .owner = THIS_MODULE,
        .read  = mp3_read,
        .write = mp3_write,
};

static const struct file_operations mp3_char_device_fileoperation = {
        .owner = THIS_MODULE,
        .open = mp3_char_device_open,
        .release = mp3_char_device_release,
        .mmap = mp3_char_device_mmap,
};


MODULE_LICENSE("GPL");
MODULE_AUTHOR("17");
MODULE_DESCRIPTION("CS-423 MP3");

// module "mp3" initialization, mp3_init called during initialization
static int __init mp3_init(void){
#ifdef DEBUG
    printk(KERN_ALERT "********************   MP3 MODULE LOADING   ********************\n");
#endif
    int ret_num;

    /* Create file /proc/mp3/status */
    proc_dir = proc_mkdir(DIRECTORY, NULL);
    if (proc_dir == NULL){
        printk(KERN_ALERT "proc directory created failed!\n");
        return -ENOMEM;
    }
    proc_entry = proc_create(FILENAME, 0666, proc_dir, & mp3_fileoperation);
    if (proc_entry == NULL){
        printk(KERN_ALERT "proc created failed!\n");
        return -ENOMEM;
    }

    /* Create and initialize mp3_tast_struct_list_head */
    INIT_LIST_HEAD(&mp3_task_struct_list_head);
    /* Create mp3_work_queue */
    mp3_work_queue = create_workqueue("mp3_work_queue");
    if(mp3_work_queue == NULL){
        printk("init work queue success.\n");
        return -ENOMEM;
    }
    mp3_work = (struct delayed_work *) kmalloc (sizeof(struct delayed_work), GFP_KERNEL);
    INIT_DELAYED_WORK(mp3_work, mp3_work_function);

    /*Allocate memory for profiler buffer*/
    mp3_profiler_buffer = (unsigned long*) vmalloc(NPAGES * PAGE_SIZE);
    if(mp3_profiler_buffer == NULL){
        printk("allocate memory for profiler buffer failed\n");
        return -ENOMEM;
    }
    memset(mp3_profiler_buffer, 0, NPAGES * PAGE_SIZE);

    /*Allocate major number and minor number ranges for char device*/
    ret_num = alloc_chrdev_region(&mp3_dev_t, 0, 1, CHAR_DEVICE_NAME);
    if(ret_num < 0){
        printk("allocate major and minor number for char device failed\n");
        return -ENOMEM;
    }

    /*Initialize mp3 char device*/
    cdev_init(&mp3_cdev, &mp3_char_device_fileoperation);
    ret_num = cdev_add(&mp3_cdev, mp3_dev_t, 1);
    if(ret_num < 0){
        printk("add char device failed\n");
        return -ENOMEM;
    }

    printk(KERN_ALERT "********************   MP3 MODULE LOADED   ********************\n");
    return 0;

}

// module "mp3" unloading, mp3_exit called during unloading
static void __exit mp3_exit(void){
#ifdef DEBUG
    printk(KERN_ALERT "********************   MP3 MODULE UNLOADING   ********************\n");
#endif

    /* Remove /proc/mp3/status and its directory */
    remove_proc_entry(FILENAME, proc_dir);
    remove_proc_entry(DIRECTORY, NULL);
    printk(KERN_INFO "%s and its directory removed\n", FILENAME);

    mutex_destroy(&list_mutex);

    /* Free and destroy the list */
    struct mp3_task_struct *position, *cursor;
    list_for_each_entry_safe(position, cursor, &mp3_task_struct_list_head, list){
        list_del(&(position->list));
        kfree(position);
    }

    /*Cancel unfinished job, destroy work queue*/
    cancel_delayed_work(mp3_work);
    flush_workqueue(mp3_work_queue);
    destroy_workqueue(mp3_work_queue);

    /*Destroy char device*/
    cdev_del(&mp3_cdev);
    unregister_chrdev_region(mp3_dev_t, 1);

    /*Free profiler buffer*/
    vfree(mp3_profiler_buffer);

    printk(KERN_ALERT "********************   MP3 MODULE UNLOADED   ********************\n");
}


// Register init and exit functions
module_init(mp3_init);
module_exit(mp3_exit);
